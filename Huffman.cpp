//Francisco Romero
//Cs 220
//Professor: Akira Kawaguchi

#include<iostream>
#include<string>
#include<map>
#include<fstream>
#include<bitset>
#include"Huffman.h"
namespace HuffmanCoding{
	/*The default constructor Makes every new node a leaf setting its right and left pointer to NULL and setting the leaf check as true. Since it has not
	been associated with any character yet its frequency count for the character in the file is zero*/
	Node::Node(){
		this->right		= NULL;
		this->left		= NULL;
		this->leaf		= true;
		this->frequency = 0;
	}
	/*This contructor is incase we want to give children to a parent Node, this sets the 2 chosen Nodes to be the left and right of the parent node
	setting leaf to false because this new Node is no longer a leaf since it has two child Nodes, and also setting frequency count to the sum of 2 childs 
	frequencies.*/
	Node::Node(Node* left, Node* right){
		this->right		= right;
		this->left		= left;
		this->leaf		= false;
		this->frequency = left->frequency + right->frequency;
	}
	/*Used this to give a better name to the type leaf, could of named the variable Test_if_leaf but it is longer than leaf when it comes to using it
	in our functions*/
	bool Node::Test_if_leaf(){
		return this->leaf;
	}
	/*This is used to associate a frequency count to a Node (Frequency count = how many times a character appears in a File... ex: "Hello" would need 4 Nodes, Node1:'H',Node2:'e',Node3:'l',Node4:'o', where 1 would have frequency
	1 , 2 would have frequency 1, 3 would say 2 and finally Node 4 would have frequency value 1*/
	void Node::setWeight(int a){
		this->frequency = a;
	}
	/*Not used in our function but it is an example of how to overload '=' when dealing with our class. for example Node A = Node B*/
	Node Node::operator=(Node A){
		this->letter	= A.letter;
		this->frequency = A.frequency;
		this->right		= A.right;
		this->left		= A.left;
		this->leaf		= A.leaf;
		return			*this;
	}
	/*Used for priority queues when comparing node a and b (by reference because we are not modifying anything so by reference would be more efficient
	since a copy is not needed). If Node A's frequency is higher it returns true else false, the priority queue uses this to sort its contents.*/
	bool Comparitor::operator()(Node &a, Node &b){
		return a.frequency > b.frequency;
	}
	/*Not used either but shows how to do a copy constructor between 2 Nodes.*/
	void Node::Copy(Node * Left, Node * Right)
	{
		left		= Left;
		right		= Right;
		frequency	= Left->frequency + Right->frequency;
		leaf		= false;
	}
	/*opens a file, since some compilers cant used a string as a filename we have to convert it to a c string hence Filename.c_str(), c_str converts a 
	string into a C string. We use seekg to place the cursor in the file to its endpoint "File.end" and then tellg tells us how far from the beggining is the
	cursor, since we moved it to the end this will tell us the file size*/
	int FileSize(std::string Filename){
		std::ifstream File(Filename.c_str());
		File.seekg(0, File.end);
		return	File.tellg();
	}
	/*In our main function we derived our root , placed our Code (initially empty) and corresponding letters to a map by pairing them and inserting.
	
	The main idea of the tree is that the bits 1's and 0's will guide us back to 
	the original letter, if we encounter a 1 it means we move right through the Nodes, if we have a 0 we move left hence we recursively go through the tree
	assigning every letter a sequence of 1's and 0's telling us which order of moving down the left branch or right branch did the iterator move to reach 
	the specific character. Once we have found a letter we place the sequence of 0's and 1's back into the map this time replacing the empty string of Code
	currently in the map by the new one EX:
															Root
											0:Left						1:Right
									0:Left(a)	1:Right(b)		0:Left(c)	1:Right(D)
									
	Here D would have the code 11 since we moved right twice to reach it, C would have 10 since we 1st moved right from the root and then left to reach it
	and so on. A = 00, B = 01*/
	void genCode(Node* root, std::map<char, std::string> &Codes, std::string Code){//default value for Code only goes in header file
		if (root->Test_if_leaf()){//We found a leaf/character
			root->code = Code;
			Codes.insert(std::pair<char, std::string>(root->letter, root->code));
			return;
		}
		genCode(root->right, Codes, Code + "1");
		genCode(root->left, Codes, Code + "0");
	}
	/*we open the file again as a c_str() input.get() extracts just 1 character from the file at a time. characters is used to keep track of how many items
	we extracted from the file / size of file. While (buffer != EOF) is while we do not extract the End Of File character from the file (present at the end
	of all the files). Everytime we successfully extract a character we add 1 to the characters current value. int(buffer) converts the character into its
	decimal ascii value so we know where to place it in the buffer for example ascii value 33 would be placed in position 33. So later if see position
	33 is not zero it means it appears in our file a certain amount of times. We end the loop by getting a different character from the file*/
	unsigned long freqCount(unsigned long* frequencies, std::string File){
		std::ifstream input(File.c_str());
		char buffer = input.get();
		unsigned long characters = 0;
		while (buffer != EOF){
			++characters;
			++frequencies[int(buffer)];
			buffer = input.get();
		}
		input.close();
		return characters;
	}
	/*We will now encode the whole file. Since our file is located in our buffer, we check character by character and find its corresponding bit stream
	appending it to a string. Not yet useful since it is a string and not really individual bits. Returns the encoded buffer as a string.*/
	std::string encode(std::map<char, std::string> &Codes, char * source, int bufferSize){
		std::string finalCode;
		for (int i = 0; i < bufferSize; ++i){
			finalCode += Codes.find(source[i])->second;
		}
		return finalCode;
	}
	/*based on the idea to encode, bitset<x> makes a type of x bits. Here 32 bits so deocodingP will make room to pack 32 bits into it. Since we will 
	start at the root and move down the tree we need to make a copy of the root so we do not lose track of it once we move to a leaf (It is static in the idea that 
	the file was too large to store the whole thing we would need to run the function multiple times, static would keep the current position of temp when the
	file ends so that it can continue from where it left of before) . maxBits will be used to keep track of how many bits we have in our encoded file. This is in
	the case where the end of the file could not fill the last 8 bytes exactly for example xxxxxxxx xxxx~~~~ where ~~~~ are useless bits written because
	our file could not fill it up. the BitCount will stop the loop once it copies only useful bits hence ending at the last x
	
	LOOP:
		source has the contents of our encoded file in bits. each item in the array stores 32bits since the type of unsigned long is 32 bits. Once we equate
		it to decodingP it will convert it to its appropriate bits. We will analize all 32 bits 0->31. starting from the most significant (left end). 
		decodingP.test(i) tests the ith bit and sees if its 1 (true) or 0 (false). If it was a 1 move right through the tree if its 0 move left. we keep
		testing if we have reached a leaf. If we do store the letter in the decompressed file and move the temp pointer back to the top of the tree. After
		each bit increase the bit counter to see how many bits we have read so far. If we have read all useful bits (maxbits) stop decoding*/
	void decode(unsigned long * source, Node root, int sourceSize, unsigned long maxBits, std::string Output){
		std::bitset<32>	decodingP;
		static Node	*temp = &root;
		static unsigned long BitCount;
		std::ofstream output(Output.c_str());
		for (int i = 0; i < sourceSize; ++i){
			decodingP = source[i];
			for (int i = 31; i >= 0; --i){
				if (decodingP.test(i) == true){
					temp = temp->right;
				}
				else{
					temp = temp->left;
				}
				if (temp->Test_if_leaf()){
					output << temp->letter;
					temp = &root;
				}
				++BitCount;
				if (BitCount == maxBits) break;
			}
			if (BitCount == maxBits) break;
		}
		output.close();
	}
	/*1st we calculate the size of the file (Compressed), open the compressed file in binary mode and read all of its contents. Read takes two arguments
	char* and a size (number of characters to read here sizeof(unsigned long) * index number of bits). An array is a sequential set of memory. We casted our 
	unsigned long* pointer to a char* pointer because the arguments for read function does not allow for unsigned long* argument. We will store this as if
	it were a character array but since an array is a sequential set of memory when this function is done it will rearange each index in the array back to 32
	bits  for example 1010101010101010101010101010101010101010 when we cast it, it will store it like this 1010101010101010 | 1010101010101010
	once we are done it turns back to a unsigned long* pointer setting it back to 1010101010101010101010101010101010101010 not seperated anymore*/
	void decodeFile(Node* Root, int index, unsigned long Bits, std::string CompressedFile, std::string output){
		unsigned long bufferSize = FileSize(CompressedFile.c_str());
		std::ifstream inputS(CompressedFile.c_str(), std::ios::binary);
		unsigned long* data = new unsigned long[bufferSize];
		inputS.read((char*) data, sizeof(unsigned long) *index);
		inputS.close();
		decode(data, *Root, index, Bits, output);
		std::cout << "Done Decoding this File: " << std::endl;
		delete [] data;
	}
	/*open compressed file in binary node so we can write to the files in terms of bits instead of characters. charCodes is an array of unsigned long
	values. It will pack 32 bits into each position in the array, index stores the current possition of 32 bits we are currently reading. counter stores how many
	bits we have packed. stringlen is used to limit how many 0's and 1's we will pack based on our string.*/
	void encodeFile(std::string decodingString, unsigned long ArrayOfBits, std::string CompressedFile){
		std::ofstream outputC(CompressedFile.c_str(), std::ios::binary);
		unsigned long *charCodes	= new unsigned long[ArrayOfBits];
		unsigned long buffer		= 0;
		unsigned long index			= 0;
		unsigned long counter		= 0;
		unsigned long stringLen		= decodingString.length();
		/*look at the 1st character in the string of 1's and 0's see if its a '1' if so or the buffer with a 1 if its a '0' or it with a 0. For example
		buffer initially = 0 , if our string is "10101010" we load one character at a time 1st iteration is load 1. Buffer or 1 = 1. increase counter and
		shift buffer left by 1 (multiply by 2) now buffer is 2 (10) the 1 was shifted one position left. then load another string character being 0. 10 or 0
		would be 10. shift again 100 if we are not done yet. Once we have packed 32 bits we equare it to an index in the charCodes array and test if the 
		counter is divisible by 32 if so we packed 32 bits already and store the value in the array. If we reached the end of the string we take care of the final
		the last unfilled 32 bits. 32-counter%32 shifts the bits to keep it in the left side of the buffer since when we read we read left to right but when
		we read bits we read from right to left*/
		for (int i = 0; i < stringLen; ++i){
			buffer <<= 1;
			buffer |= decodingString[i] == '1' ? 1 : 0;
			++counter;
			if (counter % 32 == 0){
				charCodes[index++] = buffer;
			}
			if (((i + 1) == stringLen) && counter & 8 != 0){
				buffer <<= (32 - counter % 32);
				charCodes[index++] = buffer;
			}
		}
		std::cout << "Done Compressing file" << std::endl;
		outputC.write((char*) charCodes, sizeof(unsigned long) *index);// write all of our stored bits
		delete [] charCodes;
		outputC.close();
	}
}