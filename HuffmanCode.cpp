//Francisco Romero
//Cs 220
//Professor: Akira Kawaguchi

#include<iostream>
#include<string>
#include<fstream>
#include<map>
#include<vector>
#include<queue>
#include<ctime>
#include"Huffman.h"

int main(){

	clock_t Begin_Time;											//We will time our encoding and decoding algorithm using an int to keep track of initial time
	unsigned long frequencies[256] = { 0 };						//256 characters in the ascii table, each positin in the array refers to its ascii value initially counted 0 times empty file
	
	std::priority_queue<HuffmanCoding::Node, std::vector<HuffmanCoding::Node>, HuffmanCoding::Comparitor> pq1; //used to sort Nodes giving priority to our least frequent characters (later used). Will pop the least occured character and so forth everytime we pop from the queue leaving the most occured one for last
	std::map<char, std::string> Codes;							//Stores the packed bits for each character in the file along with its character value later used.
	std::string FileName;										//Name of file which is to be compressed
	std::string OutputFile;										//Name of deCompressed File, reads our compressed file and tries to get back our original file back
	std::string CompressedFile;									//Name of Compressed File

	std::cout << "Enter your input File name to Compress: ";
	std::cin >> FileName;
	std::cout << "Enter the name of your Compressed File: ";
	std::cin >> CompressedFile;
	std::cout << "Enter the name of your output File: ";
	std::cin >> OutputFile;
	std::cout << std::endl;
	
	register unsigned long bufferSizeC = HuffmanCoding::freqCount(frequencies, FileName);//counts the instance of every letter in our file and keeps track of it in our array and returns the total number of characters in our file
	
	std::ifstream inputFile(FileName.c_str());									//opens our input file to read only i(input)fstream meaning we can only read from it its input we cannot write to it
	if (inputFile.is_open() == false) return 0;									//could not open file terminate
	else{ 
		std::cout << "File Opened SuccessFully!\n";								//Managed to open our file Successfully we may continue now
	}
	char* bufferC = new char[bufferSizeC+1];									//We want to make a dynamic array of bufferSizeC+1 because we want to have enough room for the NULL terminating character
	inputFile.read(bufferC, bufferSizeC);										//store the entire input file contents onto our new array of characters, so far no NULL terminating character is present so we might read past our array
	inputFile.close();															//Once we are done with our file close them properly
	bufferC[bufferSizeC] = '\0';												//NULL terminate our buffer making sure that we never read past the buffer
	std::cout << "Buffer contains: " << bufferC << "\n";

	unsigned long counter	= 0;												// total characters in the file. Used to determine File Size
	unsigned long totalChar = 0;												// total unique characters in the file. Used to count how many of the ascii characters we actually used so we can ignore non used characters

	for (int i = 0; i < 256; ++i){												//go through our array of ascii characters and see which ones occur in our file, if their is a count bigger than 0 that means it atleast appeared once so take note of its presence
		if (frequencies[i]>0) {
			++totalChar;														// Keep track of unique characters
			counter += frequencies[i];											// keep track of every instance of every character used
		}
	}

	std::cout << "Total characters:  " << counter << " \nUnique characters: " << totalChar << std::endl;

	int totalNodes = (totalChar - 1) * 3;										//This formula came about by manually creating the huffman tree taking note how many nodes are needed to create a tree of N characters (N-1)*3 
	HuffmanCoding::Node *NodeArray = new HuffmanCoding::Node[totalNodes];		//create an Array of Nodes to store our tree data in (occurences/frequency of character,characters)

	int j = 0;																	//keeps track of the index in our frequency table
	for (int i = 0; i < totalChar; ++i){
		while (frequencies[j] == 0) ++j;										//if our table points to an unused character skip it untill we find one that is being used in our file
		NodeArray[i].setWeight(frequencies[j]);									//if we found a character being used make our i'th node store its weight/frequency/occurences
		NodeArray[i].letter = char(j);											//if we found a character being used make our i'th node store its letter/character
		++j;																	//move on to the next character in the table
	}

	for (int i = 0; i < totalChar; ++i){										//once our nodes are all set push them into our queue which will automatically sort them based on occurences
		pq1.push(NodeArray[i]);
	}

	int parent;																	//used to keep track of when to create a parent Node
	for (int i = 0, parent = 1; i < totalNodes; ++i, ++parent){					//i is used as our index to which node we are refering to, parent is used to keep track of every 3rd step where we will create our parent node, step 1 is choosing an element from our priority queue and step 2 is choosing a 2nd element, step 3 is making a parent node to combine these
		if (parent == 3){														//We are at step 3
			NodeArray[i].Copy(&NodeArray[i - 2], &NodeArray[i - 1]);			//Combine the last 2 elements in the array
			parent = 0;															//reset the parent to 0 because it will be set back to 1 once this is done, continuing from parent = 1
			pq1.push(NodeArray[i]);												//push this new node onto our array of nodes
		}
		else{
			NodeArray[i] = pq1.top();											//we are not in step 3 so we must be in step 1 or 2 based on parents value
			pq1.pop();															//after storing our priority node in our node array we want to pop it away from the queue
		}
	}
	register HuffmanCoding::Node *Root = &NodeArray[totalNodes - 1];			//Store the last node in the array into the root node since the last popped element of the queue is the most frequent (top of the tree/root)
	HuffmanCoding::genCode(Root, Codes);										//generates the code for each character by transversing through the huffman tree

	std::string EncodedString;													//used to keep track of the encoded file as a string 
	std::cout << "BufferSize is: " << bufferSizeC << std::endl << std::endl;
	EncodedString = HuffmanCoding::encode(Codes, bufferC, bufferSizeC);			//appends the bits of 0's and 1's as a string using map function, not yet usefull because 0 as a string is 8 bits not 1 like we want
	unsigned long BitLength = 32;												//we are storing 32 bits at a time
	unsigned long ArrayOfBits = (EncodedString.length() / BitLength) +			//calculates how many times we can pack 32 bits by deviding our entire number of 1's and 0's and dividing it by 32 bits + possibility of having left over bits from 0-30 (less than 32 bits left)
		(EncodedString.length() % BitLength ? 1 : 0);							//each 32 bits is going to be stored as a 32 bit integer so this calculates how many 32 bit integers will be needed to store all the bits

	Begin_Time = clock();														//We are about to start encoding so begin saving the start time

	HuffmanCoding::encodeFile(EncodedString, ArrayOfBits, CompressedFile);		//uses string of 1's and 0's (EncodedString) to convert it to actual bits by using ArrayOfBits # of 32 bit integers to store them in the CompressedFile
	std::cout << "It took\t" << float(clock() - Begin_Time) / CLOCKS_PER_SEC * 1000 << " msec to Compress the File" << std::endl;

	Begin_Time = clock();														//starting decoding procedure, start timer again
	HuffmanCoding::decodeFile(Root, ArrayOfBits, EncodedString.length(), CompressedFile, OutputFile);//uses the huffman tree to decode the file starting from the root it will read from the compressed file ArrayOfBits # of 32 bit integers stopping when we have read up to our total of 1's and 0's determined by our string in case our last 32 bit integer only had 0-30 useful bits and the rest useless which will interfere with our data if processed
	std::cout << "It took\t" << float(clock() - Begin_Time) / CLOCKS_PER_SEC * 1000 << " msec to unCompress the File" << std::endl << std::endl;

	delete [] bufferC;															//clear out any dynamic memory used
	delete [] NodeArray;
	system("pause");
	return 0;
}