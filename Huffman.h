//Francisco Romero
//Cs 220
//Professor: Akira Kawaguchi

#pragma once //Include the file only Once
#include<iostream>
#include<string>
#include<map>


namespace HuffmanCoding{
	/*Our node class will consist of a letter (a->z) , frequency (how many times the letter occurs in the document), left/right pointers to other nodes 
	incase we want to create branches to a node), leaf (to determine if a node is a leaf or not meaning left and right node pointers are NULL)
	and code (to hold the letters bits as a string of 0's and 1's */
	class Node{
	public:
		char		letter;
		int			frequency;
		bool		leaf;
		Node		*left;
		Node		*right;
		std::string code;

		Node();
		Node(Node* left, Node* right);

		bool	Test_if_leaf();
		void	setWeight(int a);
		void	Copy(Node * Left, Node * Right);
		Node	operator=(Node A);
	};
	
	struct Comparitor{	//for priority queue
		bool operator()(Node &a, Node &b);
	};

	void genCode(Node* root, std::map<char, std::string> &Codes, std::string Code = "");

	void decode(unsigned long *source, Node root, int sourceSize, unsigned long maxBits, std::string Output);

	void decodeFile(Node *Root, int index, unsigned long Bits, std::string CompressedFile, std::string Output);

	void encodeFile(std::string decodingString, unsigned long ArrayOfBits, std::string CompressedFile);
	
	int FileSize(std::string Filename);

	unsigned long freqCount(unsigned long *frequencies, std::string File);

	std::string encode(std::map<char, std::string> &Codes, char* source,int bufferSize);
}